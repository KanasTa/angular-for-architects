import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SubjectsService {
  private subject$: BehaviorSubject<string>;
  public observable$: Observable<string>;

  constructor() {
    this.init();
  }

  init() {
    this.subject$ = new BehaviorSubject<string>('My initial string');
    this.observable$ = this.subject$.asObservable();

    setInterval(() => {
      const date = new Date();
      const time = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
      this.subject$.next(time);
    }, 3000);
  }
}
