import { EntityMetadataMap } from "@ngrx/data/src/entity-metadata/entity-metadata";

const entityMetadata: EntityMetadataMap = {
  Customer: {},
  Order: {}
};

const pluralNames = {
  Goose: 'Geese' // Would use Geese for the plural version
};

export const entityConfig = {
  entityMetadata,
  pluralNames
};