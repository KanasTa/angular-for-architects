import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ShoppingCartService } from '../../sevices/shopping-cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  public cartItemsCount: number = 0;

  private subscriptions: Subscription = new Subscription();

  constructor(private readonly shoppingCartService: ShoppingCartService) {}

  ngOnInit(): void {
    this.subscriptions.add(
      this.shoppingCartService.shoppingCartChanged$.subscribe(
        (value) => (this.cartItemsCount = value)
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
