import { Component, OnDestroy, OnInit } from '@angular/core';
import { ShoppingCartService } from '../../sevices/shopping-cart.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  constructor(private readonly shoppingCartService: ShoppingCartService) {}

  public ngOnInit(): void {}

  public addToCart(): void {
    this.shoppingCartService.addToCart();
  }
}
