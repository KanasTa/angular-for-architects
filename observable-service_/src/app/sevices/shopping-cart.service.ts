import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ShoppingCartService {
  public shoppingCartChanged$: Observable<number>;

  private itemsInCart: number = 0;
  private shoppingCartSubject$: Subject<number> = new Subject();

  constructor() {
    this.shoppingCartChanged$ = this.shoppingCartSubject$.asObservable();
  }

  public addToCart() {
    this.itemsInCart++;
    this.shoppingCartSubject$.next(this.itemsInCart);
  }
}
